package shapeways;
import java.io.*;
import java.util.Scanner;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.lang.Math;

public class ArtistPairs {

	public static Map<String, Integer> buildSet(int threshold){
		Map<String, Integer> hm = new HashMap<String, Integer>();
		Map<String, Integer> counts = new HashMap<String,Integer>();
		Scanner s = null;
		try {
			s = new Scanner(new FileInputStream("Artist_lists_small.txt"), "UTF-8");
			s.useDelimiter(",|\\n");
			int defVal = 0;
			while (s.hasNext()){
				String next = s.next();
				if (!hm.containsKey(next)){
					hm.put(next,new Integer(defVal));
					counts.put(next,new Integer(1));
				} else {
				    counts.put(next,new Integer(counts.get(next)+1));
				}
			}
			Set<String> keys = counts.keySet();
			for (String sk : keys){
				if(counts.get(sk) < threshold){
					hm.remove(sk);
				}
			}
			keys = hm.keySet();
			int count = 0;
			for (String sk : keys){
				hm.put(sk,new Integer(count));
				count++;
			}

		} catch (FileNotFoundException e) {
			System.out.println("file not found.");
		} finally {
			if (s != null){
				s.close();
			}
		}
		return hm;
	}
	
	public static String[] revLookup(Map<String,Integer> hm){
		String[] toReturn = new String[hm.size()];
		Set<String> keys = hm.keySet();
		for (String s : keys){
			toReturn[hm.get(s)] = s;
		}
		return toReturn;
	}
	
	public static void printNaivePairs(int[][] pairs, String[] lookup, int minCount){
		HashSet<String> artists= new HashSet<String>();
		for(int i=0;i<pairs.length;i++){
			for(int j=0;j<pairs.length;j++){
				if(i == j) continue;
				if(pairs[i][j] + pairs[j][i] >= minCount){ //pair A,B = pair B,A
					if(!artists.contains(lookup[j]+", "+lookup[i])){
						artists.add(lookup[i]+", "+lookup[j]);
					}
				}
			}
		}
		for (String s : artists){
			System.out.println(s);
		}
	}
	public static void main(String[] args){
		Scanner s = null;
		double percentOfData = .75;
		try {
			Map<String,Integer> hm = buildSet(50);
			int[][] pairs = new int[hm.size()][hm.size()];
			s = new Scanner(new FileInputStream("Artist_lists_small.txt"), "UTF-8");
			int numExamples = 0;
			while (s.hasNextLine()){
				if(Math.random() >= percentOfData) {
					String trash = s.nextLine();
					continue;
				}
				numExamples++;
				String[] currList = s.nextLine().split(",");
				for (int i=0; i<currList.length-1;i++){
					for(int j=i+1; j<currList.length;j++){
						String artist1 = currList[i];
						String artist2 = currList[j];
						if (!hm.containsKey(artist1) || !hm.containsKey(artist2)) 
							continue;
						int val = hm.get(artist1);
						int val2 = hm.get(artist2);
						pairs[val][val2] += 1;
					}
				}
			}
			printNaivePairs(pairs,revLookup(hm),50);
		} catch (FileNotFoundException e) {
			System.out.println("file not found.");
		} finally {
			if (s != null){
				s.close();
			}
		}
	}
	
}
